import pandas
import numpy as np
import sklearn.feature_selection as feat_select
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.neural_network import MLPClassifier

best_conf_matrix = [0, False, 0, np.ndarray, 0]

def main():
    X, y = load_data()
    layer_widths = [500, 900, 1200]
    momentum = [False, True]
    for width in layer_widths:
        print("Hidden layer width: " + str(width))
        for m in momentum:
            print("Momentum: " + str(m))
            train_evaluate(X, y, width, m)
    print("\n\n\nSUMMARY\n------------------------------------------\n")
    print("Hidden layer width: " + str(best_conf_matrix[0]) + "\nMomentum: " +
          str(best_conf_matrix[1]) + "\nFeatures number: " + str(best_conf_matrix[2]))
    print("Confusion matrix: ")
    print(best_conf_matrix[3])
    print("\nScore: " + str(best_conf_matrix[4]))

# read data
def load_data():
    dataframe = pandas.read_excel('inne.csv')
    array = dataframe.values
    y = array[:, 59]  # class columns
    X = array[:, 0:58]  # features column
    return X, y.astype(np.int)

# feature scoring
def feature_selection(X, y, n_best=58):
    test = feat_select.SelectKBest(score_func=feat_select.f_classif, k=n_best) #ANOVA
    fit = test.fit(X, y.astype('int'))
    fit_x = test.transform(X)
    # print scores
    scores = []
    for i in range(1, 58):
        scores.append([i, fit.scores_[i]])
    scores = sorted(scores, key=lambda item: item[1], reverse=True)
    # for i in scores:
    #     print(i[0], round(i[1], 3))
    return fit_x, scores


def train_evaluate(X, y, hidden_layer_width=900, momentum=True):
    for i in range(1, 8):
        global best_conf_matrix
        fit_x, _ = feature_selection(X, y, i)
        kf = RepeatedStratifiedKFold(2, 5, random_state=42)
        if momentum:
            mlp = MLPClassifier(hidden_layer_sizes=(hidden_layer_width,), max_iter=1000, alpha=0.0001, momentum=0.9,
                                nesterovs_momentum=True, solver='sgd', verbose=False, tol=0.0001)
        else:
            mlp = MLPClassifier(hidden_layer_sizes=(hidden_layer_width,), max_iter=1000, alpha=0.0001, momentum=0.0,
                                solver='sgd', verbose=False, tol=0.0001)
        val_acc_features = []

        for train_index, test_index in kf.split(fit_x, y):
            x_train, x_test = fit_x[train_index], fit_x[test_index]
            y_train, y_test = y[train_index], y[test_index]

            mlp.fit(x_train, y_train)

            prediction = mlp.predict(x_test)
            conf_mat = confusion_matrix(y_test, prediction)
            s = mlp.score(x_test, y_test)
            if best_conf_matrix[4] < s:
                best_conf_matrix = [hidden_layer_width, momentum, i, conf_mat, s]

            val_acc_features.append(s)
        print("Mean score for feature: " + str(i) + " " + str(np.mean(val_acc_features)))

if __name__ == "__main__":
    main()