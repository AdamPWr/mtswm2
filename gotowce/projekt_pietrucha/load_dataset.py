import numpy as np
import csv
from sklearn.datasets.base import Bunch


def load_dataset(file_path):
    with open(file_path) as csv_file:
        data_file = csv.reader(csv_file)
        temp = next(data_file)
        n_samples = int(temp[0])
        n_features = int(temp[1])
        data = np.empty((n_samples, n_features))
        target = np.empty((n_samples,), dtype=np.int)

        for i, sample in enumerate(data_file):
            if sample[1] == 'Male':
                sample[1] = 0
            else:
                sample[1] = 1

            try:
                data[i] = np.asarray(sample[:-1], dtype=np.float64)
                target[i] = np.asarray(sample[-1], dtype=np.int)
            except Exception as e:
                print(e)
                print("Sample number: " + str(i))


    return Bunch(data=data, target=target) # feature_names=['X', 'X2']