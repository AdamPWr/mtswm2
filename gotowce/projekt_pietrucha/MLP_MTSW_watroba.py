import numpy as np
import warnings
from sklearn.neural_network import MLPClassifier
from load_dataset import load_dataset
from sklearn.model_selection import KFold


def warn(*args, **kwargs):
    pass


warnings.warn = warn


def get_column(name):
    return X[:, dataset.feature_names.index(name)]


dataset = load_dataset('ILPDcp.csv')
dataset.feature_names = ['AGE', 'GENDER', 'TB', 'GB', 'ALKALINE', 'ALAMINE', 'ASPARTATE', 'TP', 'ALB', 'A/G']
feature_ranking = ['GB', 'TB', 'ALKALINE', 'A/G', 'ALAMINE', 'ALB', 'ASPARTATE', 'AGE', 'GENDER', 'TP']

X = dataset.data
Y = dataset.target

set_momentum = [0, 1]
set_hidden_layer_sizes = [3, 10, 20]
set_cross_validation_folds = 5

# Get first column
X_feature = get_column('GB')
X_feature = np.c_[X_feature, get_column('GB')]
X_feature = np.delete(X_feature, 1, 1)

for momentum in set_momentum:
    for hidden_layer_size in set_hidden_layer_sizes:
        for feature in feature_ranking:
            if feature is not 'GB':
                X_feature = np.c_[X_feature, get_column(feature)]

            est = MLPClassifier(hidden_layer_sizes=(hidden_layer_size,), learning_rate='adaptive',
                                solver='sgd', verbose=False, momentum=momentum, random_state=0)
            kf = KFold(n_splits=set_cross_validation_folds)

            score = []
            for train_indices, test_indices in kf.split(X_feature):
                est.fit(X_feature[train_indices], Y[train_indices])
                score.append(est.score(X_feature[test_indices], Y[test_indices]))

            print(f'momentum: {momentum}')
            print(f'hidden_layer_size: {hidden_layer_size}')
            print(feature_ranking[:(feature_ranking.index(feature) + 1)])
            print(score)
            print("Average: " + str(round(sum(score) / len(score), 2)))




