#importing libraries
from sklearn.datasets import load_boston
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
from load_dataset import load_dataset

#Loading the dataset
x = load_dataset('ILPD.csv')
x.feature_names = ['AGE', 'GENDER', 'TB', 'GB', 'ALKALINE', 'ALAMINE', 'ASPARTATE', 'TP', 'ALB', 'A/G']
feature_names = x.feature_names

df = pd.DataFrame(x.data, columns=x.feature_names)
df["SELECTOR"] = x.target
X = df.drop("SELECTOR", 1)  # Feature Matrix
y = df["SELECTOR"]          # Target Variable
df.head()

cor = df.corr()

cor_target = cor["SELECTOR"]
#print(abs(cor_target.sort_values(ascending=False).drop("SELECTOR")))

