# Load libraries
from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import chi2
from load_dataset import load_dataset

#Loading the dataset
x = load_dataset('ILPD.csv')
x.feature_names = ['AGE', 'GENDER', 'TB', 'GB', 'ALKALINE', 'ALAMINE', 'ASPARTATE', 'TP', 'ALB', 'A/G']

# Create features and target
X = x.data
y = x.target

# Create an SelectKBest object to select features with two best ANOVA F-Values
fvalue_selector = SelectKBest(f_classif, k=1)

# Apply the SelectKBest object to the features and target
X_kbest = fvalue_selector.fit_transform(X, y)

# Show results
print('Original number of features:', X.shape[1])
print('Reduced number of features:', X_kbest.shape[1])